from flask import Flask

app = Flask(__name__)

@app.route("/")

def run():
    return "Guten Tag!!!"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("8888"), debug=True)